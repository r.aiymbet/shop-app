import UIKit


class ItemViewController: UIViewController {
    @IBOutlet weak var itemImage: UIImageView!
    @IBOutlet weak var oldPrice: UILabel!
    @IBOutlet var newPrice: UILabel!
    @IBOutlet weak var discountLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    
    
    public var object: Item?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        itemImage.image = UIImage(named: "\(object!.image!)")
        
        let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: "\(object!.price!) USD")
            attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 2, range: NSMakeRange(0, attributeString.length))
        oldPrice.attributedText = attributeString
        newPrice.text = "\(Float(object!.price!) * Float(object!.discount!) / 100) USD"
        discountLabel.text = "\(object!.discount!)%"
        descLabel.text = "\(object!.description!)"
        titleLabel.text = "\(object!.title!)"
    }
    
    @IBAction func addToCart(_ sender: Any) {
        let temp = Item(image: (object?.image!)!, title: (object?.title!)!, description: (object?.description!)!, price: (object?.price!)!, discount: (object?.discount!)!)
        CartViewController.cartItems.append(temp)
        let alert = UIAlertController(title: "Done", message: "Added to cart", preferredStyle: .alert)
        
        let action1 = UIAlertAction(title: "Dismiss", style: .default) { _ in
            self.navigationController?.popToRootViewController( animated: true)
        }
        
        let action2 = UIAlertAction(title: "Go to Cart", style: .default) { (_) in
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "cartVC") as! CartViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }
        alert.addAction(action1)
        alert.addAction(action2)
        present(alert, animated: true)
    }
    
    
}
