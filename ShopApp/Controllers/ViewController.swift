import UIKit



class ViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet var tableSC: UISegmentedControl!
    static var items = [Item]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: "ItemTableViewCell", bundle: nil), forCellReuseIdentifier: "ItemTableViewCell")
        showItems()
    }
    
    private func showItems() {
        let item1 = Item(image: "book1", title: "Book One", description: "Sample description of book", price: 15.50, discount: 20)
        let item2 = Item(image: "book2", title: "Book Two", description: "Another description", price: 10.00, discount: 10)
        let item3 = Item(image: "book3", title: "Book Three", description: "One more description", price: 5.50, discount: 50)
        let item4 = Item(image: "book4", title: "Book Four", description: "Description", price: 6.30, discount: 10)
        let item5 = Item(image: "book5", title: "Book Five", description: "Informative Description", price: 3.20, discount: 50)
        
        ViewController.items = [item1, item2, item3, item4, item5]
    }
    
    
    @IBAction func gridDidChosen(_ sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 1 {
            sender.selectedSegmentIndex = 0
            let vc = storyboard?.instantiateViewController(withIdentifier: "collectionVC") as! CollectionViewController
            navigationController?.pushViewController(vc, animated: true)
            
        }
    }
    
    private func calculateDiscount(id: Int) -> Float {
        return (Float(ViewController.items[id].price!) * Float(ViewController.items[id].discount!) / 100)
    }
    
    
    func reviewItem(_ id: Int) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "itemVC") as! ItemViewController
        vc.object = Item(image: ViewController.items[id].image!, title: ViewController.items[id].title!, description: ViewController.items[id].description!, price: ViewController.items[id].price!, discount: ViewController.items[id].discount!)
        
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func goToCartVC(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "cartVC") as! CartViewController
        navigationController?.pushViewController(vc, animated: true)
    }
    
}

extension ViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ViewController.items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ItemTableViewCell") as! ItemTableViewCell
        let index = ViewController.items[indexPath.row]
        cell.imgView.image = UIImage(named: index.image!)
        cell.titleLabel.text = index.title
        cell.descLabel.text = index.description
        cell.priceLabel.text = "\(Int(calculateDiscount(id: indexPath.row))) USD"
        cell.discountLabel.text = "\(index.discount!)%"
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        reviewItem(indexPath.row)
    }
    
    
    
    
}

