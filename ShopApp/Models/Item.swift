import Foundation

public class Item: Equatable {
    public static func == (lhs: Item, rhs: Item) -> Bool {
        return true
    }
    
    var image: String?
    var title: String?
    var description: String?
    var price: Float?
    var discount: Int?


    public init(image: String, title: String, description: String, price: Float, discount: Int) {
        self.image = image
        self.title = title
        self.description = description
        self.price = price
        self.discount = discount
        
    }
}


